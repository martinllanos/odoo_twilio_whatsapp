{
    'name': "Twilio Whatsapp for Followup",
    'summary': """ This App has the ability to send Whatsapp using twilio from Followup""",
    'version': '10.0.1.0.0',
    'category': 'Whatsapp app',
    'website': "dsasoftware.com.ve",
    'author': "DSA Software SG, C.A.",
    'license': 'AGPL-3',
    'installable': True,
    'application': False,
    'images': ['images/main_screenshot.png'],
    'depends': ['twilio_whtspp','account_followup'],
    'data': [
         'views/account_followup_view.xml',
         'views/account_followup_customers.xml'
    ],
}
