# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from functools import reduce
from lxml import etree

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
from odoo.tools.misc import formatLang

class FollowupLine(models.Model):


    _inherit = 'account_followup.followup.line'

    send_whatsapp = fields.Boolean('Send an Whatsapp', default=True,
                                help="When processing, it will send an Whatsapp")
    
class ResPartner(models.Model):
    
    _inherit = 'res.partner'

    @api.multi
    def do_partner_whatsapp(self):        
        ctx = self.env.context.copy()
        ctx['followup'] = True        
        template = 'account_followup.email_template_account_followup_default'
        unknown_mails = 0        
        for partner in self:
            partners_to_whatsapp = [child for child in partner.child_ids if
                                 child.type == 'invoice' and child.mobile_whatsapp and child.enabled_whatsapp]
            if not partners_to_whatsapp and partner.mobile_whatsapp and partner.enabled_whatsapp:
                partners_to_whatsapp = [partner]  
            print('partners_to_whatsapp:',partners_to_whatsapp)     
            if partners_to_whatsapp:
                level = partner.latest_followup_level_id_without_lit
                print('level:',level)
                for partner_to_whatsapp in partners_to_whatsapp:
                    print('level whatsapp:',level.send_whatsapp)
                    if level and level.send_whatsapp:
                        print('partner_to_whatsapp:',partner_to_whatsapp.name)
                        partner_to_whatsapp.send_message_whatsapp('Deuda Pendiente')
                    else:
                        mail_template_id = self.env.ref(template)
                        partner_to_whatsapp.send_message_whatsapp('Deuda Pendiente - Sin nivel whatsapp')
                print('partner:',partner)
                if partner not in partners_to_whatsapp:
                    partner.message_post(body=_(
                        'Overdue whatsapp sent to %s' % ', '.join(
                            ['%s <%s>' % (partner.name, partner.mobile_whatsapp) for
                             partner in partners_to_whatsapp])))                        
            else:
                unknown_mails = unknown_mails + 1 
                action_text = _("Whatsapp not sent because of whatsapp number "
                                "of partner not filled in")  
                if partner.payment_next_action_date:
                    payment_action_date = min(
                        fields.Date.today(),
                        partner.payment_next_action_date)
                else:
                    payment_action_date = fields.Date.today()
                if partner.payment_next_action:
                    payment_next_action = \
                        partner.payment_next_action + " \n " + action_text
                else:
                    payment_next_action = action_text
                partner.with_context(ctx).write(
                    {'payment_next_action_date': payment_action_date,
                     'payment_next_action': payment_next_action})
        return unknown_mails