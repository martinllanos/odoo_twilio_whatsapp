from odoo import models, fields, api
from odoo.exceptions import Warning
#import datetime
from datetime import datetime
from dateutil.relativedelta import relativedelta

class ResConfigSettings(models.TransientModel):

    _inherit = 'res.config.settings'    


    twilio_account_sid = fields.Char(string="Account SID",config_parameter='twilio_whtspp.twilio_account_sid')
    twilio_auth_tocken = fields.Char(string="Auth Token",config_parameter='twilio_whtspp.twilio_auth_tocken')
    twilio_test_account_sid = fields.Char(string="Test Account SID",config_parameter='twilio_whtspp.twilio_test_account_sid')
    twilio_test_auth_tocken = fields.Char(string="Test AuthToken",config_parameter='twilio_whtspp.twilio_test_auth_tocken')    
    twilio_test_numeber = fields.Char(string="Twilio Test Number for whatsapp",config_parameter='twilio_whtspp.twilio_test_numeber')
    twilio_account_numeber = fields.Char(string="Twilio Your Number for whatsapp",config_parameter='twilio_whtspp.twilio_account_numeber')
    twilio_sandbox = fields.Boolean('Twilio sandbox',help="",config_parameter='twilio_whtspp.twilio_sandbox')
    twilio_service_id = fields.Char(string="Service ID",config_parameter='twilio_whtspp.twilio_service_id')
    twilio_test_service_id = fields.Char(string="Service ID (Test)",config_parameter='twilio_whtspp.twilio_test_service_id')
    
    def get_values(self):           
        res = super(ResConfigSettings, self).get_values()    
        conf = self.env['ir.config_parameter'].sudo() 
        res.update(
                    twilio_account_sid=conf.get_param('twilio_whtspp.twilio_account_sid', False),
                    twilio_auth_tocken=conf.get_param('twilio_whtspp.twilio_auth_tocken', False),
                    twilio_test_account_sid=conf.get_param('twilio_whtspp.twilio_test_account_sid', False),
                    twilio_test_auth_tocken=conf.get_param('twilio_whtspp.twilio_test_auth_tocken', False),
                    twilio_test_numeber= conf.get_param('twilio_whtspp.twilio_test_numeber', False),
                    twilio_account_numeber=conf.get_param('twilio_whtspp.twilio_account_numeber', False),   
                    twilio_sandbox=conf.get_param('twilio_whtspp.twilio_sandbox', True),            
                    twilio_service_id=conf.get_param('twilio_whtspp.twilio_service_id', False),
                    twilio_test_service_id=conf.get_param('twilio_whtspp.twilio_test_service_id', False),            
                    )
        return res

    def set_values(self):
        super(ResConfigSettings, self).set_values()  
        """       
        conf = self.env['ir.config_parameter'].sudo()
        conf.set_param('twilio_whtspp.twilio_account_sid', self.twilio_account_sid)
        conf.set_param('twilio_whtspp.twilio_auth_tocken', self.twilio_auth_tocken)    
        conf.set_param('twilio_whtspp.twilio_test_account_sid', self.twilio_test_account_sid)
        conf.set_param('twilio_whtspp.twilio_test_auth_tocken', self.twilio_test_auth_tocken)
        conf.set_param('twilio_whtspp.twilio_test_numeber', self.twilio_test_numeber)
        conf.set_param('twilio_whtspp.twilio_account_numeber', self.twilio_account_numeber)
        conf.set_param('twilio_whtspp.twilio_sandbox', self.twilio_sandbox)                
        conf.set_param('twilio_whtspp.twilio_service_id', self.twilio_service_id)
        conf.set_param('twilio_whtspp.twilio_test_service_id', self.twilio_test_service_id)
        """        