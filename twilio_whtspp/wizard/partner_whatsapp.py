# -*- coding: utf-8 -*-
from odoo import models, api, fields, _
from odoo.exceptions import UserError
from time import sleep

class ResPartnerWhatsapp(models.TransientModel):
    """
    This wizard will confirm the all the selected draft invoices
    """

    _name = "res.partner.whatsapp"
    _description = "Confirm message send by whatsapp"
    
    message_text = fields.Text('Message')

    @api.multi
    def message_send_whatsapp(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []

        for record in self.env['res.partner'].browse(active_ids):
            record.send_message_whatsapp(self.message_text)            
        return {'type': 'ir.actions.act_window_close'}
