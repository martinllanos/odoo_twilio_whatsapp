# -*- coding: utf-8 -*-
from odoo import models, api, _
from odoo.exceptions import UserError
from time import sleep

class AccountInvoiceConfirm(models.TransientModel):
    """
    This wizard will confirm the all the selected draft invoices
    """

    _name = "account.invoice.whatsapp"
    _description = "Confirm the selected invoices send whatsapp"

    @api.multi
    def invoice_send_whatsapp(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []

        for record in self.env['account.invoice'].browse(active_ids):
            record.send_invoice_whatsapp()            
        return {'type': 'ir.actions.act_window_close'}
