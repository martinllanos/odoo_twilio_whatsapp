# -*- coding: utf-8 -*-
import base64
import logging

from odoo import models, fields, api, _
from wand.image import Image
from wand.display import display
from wand.color import Color

_logger = logging.getLogger(__name__)

class ir_attachment(models.Model):
    _inherit='ir.attachment'

    @api.model
    def create(self, values):
        att = super(ir_attachment, self).create(values)
        if self._context.get('convert') == 'pdf2jpg' and att.mimetype == 'application/pdf':
            att.pdf2image(800,1200)
        return att

    @api.multi
    def pdf2image(self,dest_width, dest_height):
        RESOLUTION = 300
        for attachment in self:
            pdf = base64.decodestring(attachment.datas)
            img = Image(blob=pdf,resolution=(RESOLUTION,RESOLUTION))
            img.background_color = Color('white')
            img.resize(dest_width,dest_height)
            img.alpha_channel = 'remove'
            ATTACHMENT_NAME = attachment.datas_fname[:-4]   
            attachment.datas = base64.encodestring(img.make_blob(format='png'))
            attachment.datas_fname = ATTACHMENT_NAME + '.png'
            attachment.mimetype = 'image/png'
