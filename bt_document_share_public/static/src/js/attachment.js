odoo.define('bt_document_share_public.AttachmentLink', function (require) {
"use strict";

var core = require('web.core');
var Chatter = require('mail.Chatter');
var MailThread = core.form_widget_registry.get('mail_thread');
var Widget = require('web.Widget');

var AttachmentLink = Chatter.include({
    
	init: function () {
        this._super.apply(this, arguments);
        this.events = _.extend(this.events, {
        	"click .o_chatter_button_attachment_link": '_OnOpenGetAttachmentLink',
        });
    },

    _OnOpenGetAttachmentLink: function () {
        this._OpenAttachmentLink();
    },
    
    _OpenAttachmentLink: function() {
        var self = this;
        var context = {};

        if (self.context.default_model && self.context.default_res_id) {
        	context.active_model = self.context.default_model;
            context.default_res_id = self.context.default_res_id;
            context.active_id = self.context.default_res_id;
        }
        self.do_action({
            name: 'Attachments',
        	type: 'ir.actions.act_window',
            res_model: 'attachment.link',
            view_mode: 'form',
            view_type: 'form',
            views: [[false, 'form']],
            target: 'new',
            context: context,
        }, {
            on_close: function() {
                self.trigger('need_refresh');
            },
        });
        
    },
	
});

});

